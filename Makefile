CPPFLAGS=-g -pthread 
LDFLAGS=-g
LDLIBS=-lncursesw

SRC_DIR := src
OBJ_DIR := target
SRC_FILES := $(wildcard $(SRC_DIR)/*.cpp)
OBJ_FILES := $(patsubst $(SRC_DIR)/%.cpp,$(OBJ_DIR)/%.o,$(SRC_FILES))

rogue: $(OBJ_FILES)
	g++ $(LDFLAGS) -o $@ $^ $(LDLIBS)

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.cpp
	g++ $(CPPFLAGS) $(CXXFLAGS) -c -o $@ $<

clean:
	rm ${OBJ_DUR}/*.o rogue
