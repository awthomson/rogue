#include <unistd.h>                  /*  for sleep()  */
#include <locale.h>
#include <time.h>
#include <stdlib.h>
#include <stdio.h>

#include "screen.h"
#include "dice.h"
#include "pc.h"
#include "dungeon.h"

using namespace std;

int main() {

	srand (time(NULL));
	PC myChar;
	myChar.set_class(fighter);
	myChar.set_race(dwarf);
	myChar.set_subrace(mountain_dwarf);
	myChar.roll();
	myChar.debug();
	
	Dungeon myDungeon;
	myDungeon.init();
	// myDungeon.debug();

	char *locale;

    locale = setlocale(LC_ALL, "");

	Screen myScreen;
	myScreen.init();
	
	// Main game loop
	bool keep_going = true;
	while (keep_going) {
		myScreen.draw_game(myDungeon);
		char c = getchar();
		if (c == 'q')
			keep_going = false;
		if (c == '8')
			myDungeon.pos_y--;
		if (c == '2')
			myDungeon.pos_y++;
		if (c == '4')
			myDungeon.pos_x--;
		if (c == '6')
			myDungeon.pos_x++;
	}
	
	// Adios
	myScreen.destroy();

	return 1;


}
