enum PlayerClass { fighter, thief };
enum PlayerRace { human, dwarf };
enum PlayerSubRace { mountain_dwarf, hill_dwarf, high_elf };

class PC {

	public:
	void init();
	void destroy();
	void roll();
	void debug();
	void set_class(PlayerClass c);	
	void set_race(PlayerRace r);	
	void set_subrace(PlayerSubRace sr);	


	private:
	int str;
	int dex;
	int con;
	int wis;
	int chr;
	int itl;
	int level;
	int exp;
	PlayerClass cls;
	PlayerRace race;
	PlayerSubRace subrace;
	
};

