#include <stdlib.h>
#include <iostream>
#include <algorithm>

#include "dice.h"

using namespace std;

int roll(int num, int sides, int mod) {
	int total = mod;
	for (int i=0; i<num; i++)
		total += rand() % sides + 1;
	return total;
}

int highest_3_of_4() {

	int a = rand()%6+1;
	int b = rand()%6+1;
	int c = rand()%6+1;
	int d = rand()%6+1;

	if ((a<=b) and (a<=c) and (a<=d)) 
		return b+c+d;
	if ((b<=a) and (b<=c) and (b<=d)) 
		return a+c+d;
	if ((c<=a) and (c<=b) and (c<=d)) 
		return a+b+d;
	return a+b+c;
	
}

// Generate array of 6 items, sorted.
// Leave it to caller to assign to stats
void roll_stats(int *stats) {

	for (int i=0; i<6; i++)
		stats[i]=highest_3_of_4();

	std::sort(stats, stats+6);
	
}
