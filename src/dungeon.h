#ifndef __dungeon_h__
#define __dungeon_h__


#define max_x 25
#define min_x 4
#define max_y 16
#define min_y 3
// Twistyness of the tunnels 2 - 10
#define STRAIGHTNESS 4

enum DungeonType { standard };

enum db_Feature { nothing, stone, stone_floor, stone_tunnel, open_door, closed_door, stairs_down, stairs_up, fountain, barrel, statue };

struct DungeonBlock {
	db_Feature feature;
	bool is_traversable;
	bool is_known;
	bool is_visible;
};


class Dungeon {

	public:
	// Public dungeon variables
	int get_level_size_x();
	int get_level_size_y();
	int pos_x;
	int pos_y;

	// Public dungeon functions
	void init();
	void destroy();
	void set_type(DungeonType dt); 
	void debug();
	void determine_visible();
	DungeonBlock *get_level_data();

	
	private:
	DungeonBlock *level_data;
	DungeonType type;
	int num_levels;
	int current_level;

	// Level generation variables
	int level_size_x, level_size_y;
	int num_rooms;
	int max_room_size_x;
	int max_room_size_y;
	int min_room_size_x;
	int min_room_size_y;

	// Level generation functions
	bool DoFov(float x, float y);
	void set_block_visibility(int x, int y, bool is_visible);
	int how_many_adjacent(int x, int y, db_Feature db);
	int how_many_orth(int x, int y, db_Feature db);
	int how_many_diag(int x, int y, db_Feature db);
	void generate();
	void add_rooms();
	void fill_path();
	void fill(int x, int y, int order);
	void set_block_feature(int x, int y, db_Feature db);
	db_Feature get_block_feature(int x, int y);
	bool get_block_traversable(int x, int y);
	void do_doors();
	int count_horizontal(int x, int y, db_Feature db);
	int count_vertical(int x, int y, db_Feature db);
	void tidy_paths();
	void tidy_rest();	
	void place_player_randomly();

};


#endif
