#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <cmath>

#include "dungeon.h"

using namespace std;

/***********************************************************************
 * 
 * Public funtions
 * 
 ***********************************************************************/

void Dungeon::init() {
	level_size_x = 160;
	level_size_y = 60;
	num_rooms = 10;
	max_room_size_x = 30;
	max_room_size_y = 20;
	min_room_size_x = 5;
	min_room_size_y = 4;

	level_data = new DungeonBlock[level_size_x * level_size_y];
	
	generate();
	place_player_randomly();

}

void Dungeon::destroy() {
}

void Dungeon::set_type(DungeonType dt) {
	type = dt;
}

void Dungeon::determine_visible()
{
  float x,y;
  int i;
  
  for(int my=0; my<level_size_y; my++)
	for(int mx=0; mx<level_size_x; mx++)
		set_block_visibility(mx, my, false);
		
	for(i=0;i<360;i++) {
		x=cos((float)i*0.01745f);
		y=sin((float)i*0.01745f);
		DoFov(x,y);
	};
	
}

DungeonBlock *Dungeon::get_level_data() {
	return level_data;
}

/***********************************************************************
 * 
 * Private funtions
 * 
 ***********************************************************************/

bool Dungeon::DoFov(float x, float y) {
  int i;
  float ox,oy;
  ox = (float)pos_x+0.5f;
  oy = (float)pos_y+0.5f;
  for(i=0;i<15;i++) {
    set_block_visibility((int)ox, (int)oy, true);
    if (get_block_traversable((int)ox, (int)oy) == false)
      return true;
    ox+=x;
    oy+=y;
  };
  return false;
}

void Dungeon::place_player_randomly() {
	bool keep_going = true;
	while (keep_going) {
		int x = rand() % level_size_x + 1;
		int y = rand() % level_size_y + 1;
		int n = how_many_adjacent(x, y, stone_floor);
		if (n == 8) {
			pos_x = x;
			pos_y = y;
			keep_going = false;
		}
	}
	
}

int Dungeon::get_level_size_x() {
	return level_size_x;
}

int Dungeon::get_level_size_y() {
	return level_size_y;
}

// Get dungeon block at specified position
db_Feature Dungeon::get_block_feature(int x, int y) {
    return level_data[y*level_size_x+x].feature;
}

// Get dungeon block at specified position
bool Dungeon::get_block_traversable(int x, int y) {
    return level_data[y*level_size_x+x].is_traversable;
}

// Set dungeon block at specified position
void Dungeon::set_block_feature(int x, int y, db_Feature db) {
    level_data[y*level_size_x+x].feature=db;
	if (db == stone) {
		level_data[y*level_size_x+x].is_traversable = false;
    } else {
		level_data[y*level_size_x+x].is_traversable = true;
	}
}

// Set dungeon block at specified position
void Dungeon::set_block_visibility(int x, int y, bool is_visible) {
    level_data[y*level_size_x+x].is_visible=is_visible;
    
    if (is_visible)
		level_data[y*level_size_x+x].is_known=true;
}

void Dungeon::generate(void) {
    
    // Make entire dungeon stone block
    for (int y=0; y<level_size_y; y++) 
        for (int x=0; x<level_size_x; x++)
			set_block_feature(x, y, stone);
    
    add_rooms();
    fill_path();
    do_doors();
    tidy_paths();
    tidy_rest();

}


int Dungeon::how_many_adjacent(int x, int y, db_Feature db) {
    int num=0;
    if (get_block_feature(x-1, y-1) == db) num++;
    if (get_block_feature(x  , y-1) == db) num++;
    if (get_block_feature(x+1, y-1) == db) num++;
    if (get_block_feature(x+1, y  ) == db) num++;
    if (get_block_feature(x+1, y+1) == db) num++;
    if (get_block_feature(x  , y+1) == db) num++;
    if (get_block_feature(x-1, y+1) == db) num++;
    if (get_block_feature(x-1, y  ) == db) num++;
    return num;
}

int Dungeon::how_many_orth(int x, int y, db_Feature db) {
    int num=0;
    if (get_block_feature(x+1, y  ) == db) num++;
    if (get_block_feature(x-1, y  ) == db) num++;
    if (get_block_feature(x  , y-1) == db) num++;
    if (get_block_feature(x  , y+1) == db) num++;
    return num;
}

int Dungeon::how_many_diag(int x, int y, db_Feature db) {
    int num=0;
    if (get_block_feature(x-1, y-1) == db) num++;
    if (get_block_feature(x+1, y-1) == db) num++;
    if (get_block_feature(x+1, y+1) == db) num++;
    if (get_block_feature(x-1, y+1) == db) num++;
    return num;
}

// Randomly place a bunch of empty rooms on the level
void Dungeon::add_rooms() {
	
	for (int room=0; room<num_rooms; room++) {
    
		bool keep_going = true; 
		while (keep_going) {
			int x1 = rand() % level_size_x + 1;
			int y1 = rand() % level_size_y + 1;
			int block_size_x = rand() % (max_room_size_x-min_room_size_x) + min_room_size_x;
			int block_size_y = rand() % (max_room_size_y-min_room_size_y) + min_room_size_y;
			int x2 = x1 + block_size_x;
			int y2 = y1 + block_size_y;
			if ((x2>level_size_x-1) || (y2>level_size_y-1))
				continue;
		  
			bool overlap = false;
			for (int y=y1; y<y2; y++)
				for (int x=x1; x<x2; x++)
					if (how_many_adjacent(x, y, stone_floor)>0)
						overlap = true;

			if (overlap) {
				keep_going = true;
			} else {
				keep_going = false;
				for (int y=y1; y<y2; y++)
					for (int x=x1; x<x2; x++)
						set_block_feature(x, y, stone_floor);
			}
		}   
	}
}

// Fill in all the gaps with random tunnels
void Dungeon::fill_path() {
    
    // Get first position a valid path can start
    int currx = 1;
    int curry = 1;
    bool keep_going = true;
    while (keep_going) {
        if (how_many_adjacent(currx, curry, stone)==8) {
            fill(currx, curry, 0);
        } 
        currx++;
		if (currx>=level_size_x-1) {
			currx = 1;
			curry++;
			if (curry>=level_size_y-1)
				keep_going = false;
        }
    }
    
}

void Dungeon::fill(int x, int y, int order) {
    
    if ((x<1) || (y<1) || (x>=level_size_x-1) || (y>=level_size_y-1))
        return;
    
    set_block_feature(x, y, stone_tunnel);
    
    if (rand() % STRAIGHTNESS == 0) {
        order = (order + 1) % 4;
    }

    if (order == 0) {
        if ((x>2) && (how_many_orth(x-1, y, stone)>2) && (how_many_diag(x-1, y, stone)>2) && (how_many_diag(x-1, y, stone_floor)==0) && (get_block_feature(x-1, y)==stone))
            fill(x-1, y, order);
        if ((y>2) && (how_many_orth(x, y-1, stone)>2) && (how_many_diag(x, y-1, stone)>2) && (how_many_diag(x, y-1, stone_floor)==0) && (get_block_feature(x, y-1)==stone))
            fill(x, y-1, order);
        if ((x<level_size_x) && (how_many_orth(x+1, y, stone)>2) && (how_many_diag(x+1, y, stone)>2) && (how_many_diag(x+1, y, stone_floor)==0) && (get_block_feature(x+1, y)==stone))
            fill(x+1, y, order);
        if ((y<level_size_y) && (how_many_orth(x, y+1, stone)>2) && (how_many_diag(x, y+1, stone)>2) && (how_many_diag(x, y+1, stone_floor)==0) && (get_block_feature(x, y+1)==stone))
            fill(x, y+1, order);
    } else if (order == 1) {
        if ((y>2) && (how_many_orth(x, y-1, stone)>2) && (how_many_diag(x, y-1, stone)>2) && (how_many_diag(x, y-1, stone_floor)==0) && (get_block_feature(x, y-1)==stone))
            fill(x, y-1, order);
        if ((x<level_size_x) && (how_many_orth(x+1, y, stone)>2) && (how_many_diag(x+1, y, stone)>2) && (how_many_diag(x+1, y, stone_floor)==0) && (get_block_feature(x+1, y)==stone))
            fill(x+1, y, order);
        if ((y<level_size_y) && (how_many_orth(x, y+1, stone)>2) && (how_many_diag(x, y+1, stone)>2) && (how_many_diag(x, y+1, stone_floor)==0) && (get_block_feature(x, y+1)==stone))
            fill(x, y+1, order);
        if ((x>2) && (how_many_orth(x-1, y, stone)>2) && (how_many_diag(x-1, y, stone)>2) && (how_many_diag(x-1, y, stone_floor)==0) && (get_block_feature(x-1, y)==stone))
            fill(x-1, y, order);
    } else if (order == 2) {
        if ((x<level_size_x) && (how_many_orth(x+1, y, stone)>2) && (how_many_diag(x+1, y, stone)>2) && (how_many_diag(x+1, y, stone_floor)==0) && (get_block_feature(x+1, y)==stone))
            fill(x+1, y, order);
        if ((y<level_size_y) && (how_many_orth(x, y+1, stone)>2) && (how_many_diag(x, y+1, stone)>2) && (how_many_diag(x, y+1, stone_floor)==0) && (get_block_feature(x, y+1)==stone))
            fill(x, y+1, order);
        if ((x>2) && (how_many_orth(x-1, y, stone)>2) && (how_many_diag(x-1, y, stone)>2) && (how_many_diag(x-1, y, stone_floor)==0) && (get_block_feature(x-1, y)==stone))
            fill(x-1, y, order);
        if ((y>2) && (how_many_orth(x, y-1, stone)>2) && (how_many_diag(x, y-1, stone)>2) && (how_many_diag(x, y-1, stone_floor)==0) && (get_block_feature(x, y-1)==stone))
            fill(x, y-1, order);
    } else {
        if ((y<level_size_y) && (how_many_orth(x, y+1, stone)>2) && (how_many_diag(x, y+1, stone)>2) && (how_many_diag(x, y+1, stone_floor)==0) && (get_block_feature(x, y+1)==stone))
            fill(x, y+1, order);
        if ((x>2) && (how_many_orth(x-1, y, stone)>2) && (how_many_diag(x-1, y, stone)>2) && (how_many_diag(x-1, y, stone_floor)==0) && (get_block_feature(x-1, y)==stone))
            fill(x-1, y, order);
        if ((y>2) && (how_many_orth(x, y-1, stone)>2) && (how_many_diag(x, y-1, stone)>2) && (how_many_diag(x, y-1, stone_floor)==0) && (get_block_feature(x, y-1)==stone))
            fill(x, y-1, order);
        if ((x<level_size_x) && (how_many_orth(x+1, y, stone)>2) && (how_many_diag(x+1, y, stone)>2) && (how_many_diag(x+1, y, stone_floor)==0) && (get_block_feature(x+1, y)==stone))
            fill(x+1, y, order);
    }
}

void Dungeon::tidy_rest() {

	// Remove stray tunnels
	for (int y=0; y<level_size_y; y++) {
		for (int x=0; x<level_size_x; x++) {
			if ((get_block_feature(x, y)==stone_tunnel) && (how_many_orth(x, y, stone)==4))
				set_block_feature(x, y, stone);
		}
	}

	// Remove stray doors
	for (int y=0; y<level_size_y; y++) {
		for (int x=0; x<level_size_x; x++) {
			if ((get_block_feature(x, y)==closed_door) && (how_many_orth(x, y, stone)==3))
				set_block_feature(x, y, stone);
		}
	}
}

void Dungeon::tidy_paths() {
    
    int found = 1;
    while (found>0) {
        found = 0;
        for (int y=1; y<level_size_y-1; y++) {
            for (int x=1; x<level_size_x-1; x++) {
                if ( (get_block_feature(x, y) == stone_tunnel) && (how_many_orth(x, y, stone) == 3)) {
                    set_block_feature(x, y, stone);
                    found++;
                }
            }
        }
    }
    
}



void Dungeon::do_doors() {
    
    // Mark all candidate iwth a +
    for (int y=0; y<level_size_y-1; y++) {
        for (int x=0; x<level_size_x-1; x++) {
            if ((y<level_size_y-2) && ((get_block_feature(x, y)==stone_tunnel) || (get_block_feature(x,y)==stone_floor)) && (get_block_feature(x, y+1)==stone) && (get_block_feature(x, y+2)==stone_floor))
                set_block_feature(x, y+1, closed_door);
            if ((y>2) && ((get_block_feature(x, y)==stone_tunnel) || (get_block_feature(x,y)==stone_floor)) && (get_block_feature(x, y-1)==stone) && (get_block_feature(x, y-2)==stone_floor))
                set_block_feature(x, y-1, closed_door);
            if ((x>2) && ((get_block_feature(x, y)==stone_tunnel) || (get_block_feature(x,y)==stone_floor)) && (get_block_feature(x-1, y)==stone) && (get_block_feature(x-2, y)==stone_floor))
                set_block_feature(x-1, y, closed_door);
            if ((x<level_size_x<2) && ((get_block_feature(x, y)==stone_tunnel) || (get_block_feature(x,y)==stone_floor)) && (get_block_feature(x+1, y)==stone) && (get_block_feature(x+2, y)==stone_floor))
                set_block_feature(x+1, y, closed_door);
        }
    }

    // Tidy up horizontal row of doors
    for (int y=0; y<level_size_y-1; y++) {
        for (int x=0; x<level_size_x-1; x++) {
            if (get_block_feature(x, y)==closed_door) {
                int hd = count_horizontal(x, y, closed_door);
                for (int d=0; d<hd; d++)
                    set_block_feature(x+d, y, stone);
                set_block_feature(x+(hd/2), y, closed_door);
            }
        }
    }

    // Tidy up vertical coulmns of doors
    for (int y=0; y<level_size_y-1; y++) {
        for (int x=0; x<level_size_x-1; x++) {
            if (get_block_feature(x, y)==closed_door) {
                int hd = count_vertical(x, y, closed_door);
                for (int d=0; d<hd; d++)
                    set_block_feature(x, y+d, stone);
                set_block_feature(x, y+(hd/2), closed_door);
            }
        }
    }
    
}

int Dungeon::count_horizontal(int x, int y, db_Feature db) {
    int count = 0;
    while ((x<level_size_x) && (get_block_feature(x, y)==db)) {
        count++;
        x++;
    }
    return count;
}

int Dungeon::count_vertical(int x, int y, db_Feature db) {
    int count = 0;
    while ((y<level_size_y) && (get_block_feature(x, y)==db)) {
        count++;
        y++;
    }
    return count;
}
