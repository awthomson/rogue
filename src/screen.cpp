#include <iostream>
#include <stdlib.h>

#include "screen.h"
#include "dungeon.h"

using namespace std;

void Screen::init() {
	
	mainwin = initscr();
    getmaxyx(mainwin, winy, winx);
	winx--;
	winy--;
	printf("X: %d     Y: %d\n", winx, winy);
	curs_set(0);

}

void Screen::destroy() {
	endwin();
}

void Screen::draw_game(Dungeon d) {
	draw_game_border();
	draw_dungeon(d);
	refresh();
}

void Screen::draw_dungeon(Dungeon d) {
	start_color();
	init_pair(4, COLOR_BLACK, COLOR_BLACK);	// Unknown & Not Visible
	init_pair(3, COLOR_GREEN, COLOR_BLACK);	// Known but not visible
	init_pair(2, COLOR_WHITE, COLOR_BLACK); // Visible
	d.determine_visible();
	// TODO: Don't hardcode the 80,20 values
	int off_x = d.pos_x-80;
	int off_y = d.pos_y-20;
	DungeonBlock *ld = d.get_level_data();
	int lsx = d.get_level_size_x();
	int lsy = d.get_level_size_y();
	for (int draw_y=0; draw_y<winy-11; draw_y++) {
		move(draw_y+1, 1);
		for (int draw_x=0; draw_x<winx-41; draw_x++) {
			db_Feature db;
			int map_x = draw_x+off_x;
			int map_y = draw_y+off_y;
			if ((map_x>=lsx) || (map_y>=lsy) || (map_x<0) || (map_y<0)) {
				db = nothing;
			} else {
				db = ld[(map_y) * lsx + (map_x)].feature;
			}
			if (ld[map_y*lsx+map_x].is_visible == true) {
				attron(COLOR_PAIR(2));
			} else if (ld[map_y*lsx+map_x].is_known == true) {
				attron(COLOR_PAIR(3));
			} else {
				attron(COLOR_PAIR(4));
			}
			wchar_t disp_char = L'▒';
			switch (db) {
				case stone:
					disp_char = L'░';
					break;
				case stone_floor:
					disp_char = L'.';
					break;
				case stone_tunnel:
					disp_char = L'#';
					break;
				case closed_door:
					disp_char = L'◫';
					break;
				case open_door:
					disp_char = L'□';
					break;
				case stairs_down:
					disp_char = L'>';
					break;
				case stairs_up:
					disp_char = L'<';
					break;
				case fountain:
					disp_char = L'⛲';
					break;
				case barrel:
					disp_char = L'⛁';
					break;
				case statue:
					disp_char = L'☥';
					break;
				default:
					disp_char = L' ';
					break;
			}
			if  (((map_y)==d.pos_y) and ((map_x)==d.pos_x)) {
				printw("@");
			} else {
				printw("%lc", disp_char);
			}
		}
	}
}

void Screen::draw_game_border() {
	
	start_color();
	init_pair(1, COLOR_YELLOW, COLOR_BLACK);
	attron(COLOR_PAIR(1));

	int log_height = 10;
	int info_width = 40;
	
	

	// Top border
	move(0,1);
	for (int x=0; x<winx-1; x++)
		// printw("═");
		printw("━");

	// Bottom
	move(winy,1);
	for (int x=0; x<winx-1; x++)
		printw("━");

	// Top of log window
	move(winy-10,1);
	for (int x=0; x<winx-1; x++)
		printw("━");

	// Left and right border
	for (int y=1; y<winy; y++) {
		move(y,0);
		printw("┃");
		move(y,winx);
		printw("┃");
	}

	// Left edge of info panel
	for (int y=1; y<winy-10; y++) {
		move(y, winx-info_width);
		printw("┃");
	}	

	// Joinery
	move(   0,    0);	printw("┏");
	move(   0, winx);	printw("┓");
	move(winy, winx);	printw("┛");
	move(winy,    0);	printw("┗");

	move(winy-log_height,    0);	printw("┣");
	move(winy-log_height, winx);	printw("┫");

	move(              0, winx-info_width);	printw("┳");
	move(winy-log_height, winx-info_width);	printw("┻");



}
