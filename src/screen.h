#include <ncurses.h>

#include "dungeon.h"

class Screen {

	public:
	void init();
	void destroy();
	void draw_game(Dungeon d);
	void draw_dungeon(Dungeon d);

	private:

 	WINDOW *mainwin;
	int winx, winy;
	
	void draw_game_border();

};

