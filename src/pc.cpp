#include <iostream>

#include "pc.h"
#include "dice.h"

using namespace std;

void PC::init() {
}

void PC::destroy() {
}

void PC::roll() {
	int s[6];
	roll_stats(s);
	
	// Auto-assign results based on player class
	switch (cls) {
		case fighter:
			str = s[5];
			con = s[4];
			dex = s[3];
			chr = s[2];
			itl = s[1];
			wis = s[0];
			break;
		case thief:
			dex = s[5];
			chr = s[4];
			itl = s[3];
			str = s[2];
			wis = s[1];
			con = s[0];
			break;
	}
	
	// Apply race bonuses
	switch (race) {
		case dwarf:
			con += 2;
			break;
		case human:
			break;
	}
	
	// Apply subrace bonuses
	switch (subrace) {
		case mountain_dwarf:
			str += 2;
			break;
	}
}

void PC::set_class(PlayerClass c) {
	cls = c;
}

void PC::set_race(PlayerRace r) {
	race = r;
}

void PC::set_subrace(PlayerSubRace sr) {
	subrace = sr;
}

void PC::debug() {
	cout << "STR: " << str << "\n";
	cout << "CON: " << con << "\n";
	cout << "DEX: " << dex << "\n";
	cout << "CHR: " << chr << "\n";
	cout << "ITL: " << itl << "\n";
	cout << "WIS: " << wis << "\n";
	switch (race) {
	case human:
		cout << "Human\n";
		break;
	case dwarf:
		cout << "Dwarf\n";
		break;
	}
	if (cls == fighter) 
		cout << "Fighter\n";
	else if (cls == thief) 
		cout << "Thief\n";
	
}
